﻿using System;
using System.Threading;

namespace MfGames.Locking
{
    /// <summary>
    /// Defines a ReaderWriterLockSlim upgradable read lock.
    /// </summary>
    public class NestableUpgradableReadLock : IDisposable
    {
        private readonly bool lockAcquired;

        private readonly ReaderWriterLockSlim readerWriterLockSlim;

        /// <summary>
        /// Initializes a new instance of the <see cref="NestableUpgradableReadLock" />
        /// class.
        /// </summary>
        /// <param name="readerWriterLockSlim">The reader writer lock slim.</param>
        public NestableUpgradableReadLock(
            ReaderWriterLockSlim readerWriterLockSlim)
        {
            // Keep track of the lock since we'll need it to release the lock.
            this.readerWriterLockSlim = readerWriterLockSlim;

            // If we already have a read or write lock, we don't do anything.
            if (readerWriterLockSlim.IsUpgradeableReadLockHeld
                || readerWriterLockSlim.IsWriteLockHeld)
            {
                this.lockAcquired = false;
            }
            else
            {
                readerWriterLockSlim.EnterUpgradeableReadLock();
                this.lockAcquired = true;
            }
        }

        /// <inheritdoc />
        public void Dispose()
        {
            if (this.lockAcquired)
            {
                this.readerWriterLockSlim.ExitUpgradeableReadLock();
            }
        }
    }
}
