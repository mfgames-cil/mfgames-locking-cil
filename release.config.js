module.exports = {
  branches: ["main"],
  message: "chore(release): v${nextRelease.version}\n\n${nextRelease.notes}",
  plugins: [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    "@semantic-release/npm",
    [
      "semantic-release-dotnet",
      {
	    paths: ["src/Directory.Build.props"],
      },
    ],
    [
      "semantic-release-nuget",
      {
        packArguments: ["--include-symbols", "--include-source"],
        pushFiles: ["src/*/bin/Debug/*.nupkg"],
      },
    ],
    "@semantic-release/changelog",
    "@semantic-release/git",
    "@semantic-release/gitlab",
  ],
};
